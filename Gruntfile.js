module.exports = function(grunt) {
  var imageminJpegoptim = require('imagemin-jpegoptim');

  grunt.initConfig({
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            '*.css',
            '*.js',
            '*.html'
          ]
        },
        options: {
          open: false,
          proxy: 'http://127.0.0.1/bitbucket/formann-loyt',
          reloadOnRestart: true,
          ui: {
            port: 2999
          },
          watchTask: true
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true,
          minifyCSS: true,
          minifyJS: true,
        },
        files: {
          'index.html': 'dev/index.html'
        }
      },
      dev: {
        files: {
          'index.html': 'dev/index.html'
        }
      }
    },
    imagemin: {
      dynamic: {
        options: {
          use: [imageminJpegoptim({max: 90})]
        },
        files: [
          {
            expand: true,
            cwd: 'dev/img',
            src: ['*.{png,jpg,gif}'],
            dest: 'img'
          },
          {
            expand: true,
            cwd: 'dev/img_small',
            src: ['*.{png,jpg,gif}'],
            dest: 'img'
          }
        ]
      },
      dynamic2: {
        options: {
          use: [imageminJpegoptim({max: 95})]
        },
        files: [
          {
            expand: true,
            cwd: 'dev/img',
            src: ['*-main@2x.jpg'],
            dest: 'img'
          },
          {
            expand: true,
            cwd: 'dev/img_small',
            src: ['*-main.jpg'],
            dest: 'img'
          }
        ]
      }
    },
    postcss: {
      dev: {
        options: {
          map: {
            inline: true
          },
          processors: [
            require('autoprefixer')
          ]
        },
        src: 'style.css'
      },
      dist: {
        options: {
          map: true,
          processors: [
            require('autoprefixer'),
            require('cssnano')(),
            require('pixrem')(),
            require('postcss-color-rgba-fallback')(),
            require('postcss-opacity')(),
            require('postcss-pseudoelements')(),
            require('postcss-vmin')()
          ]
        },
        src: 'style.css'
      }
    },
    sass: {
      options: {
        sourceMap: true,
        includePaths: [
          'bower_components',
          'node_modules',
          'node_modules/bootstrap-sass/assets/stylesheets'
        ]
      },
      dist: {
        files: {
          'style.css': 'dev/style.scss'
        }
      }
    },
    uncss: {
      dist: {
        options: {
          ignore: [
            '.open > .dropdown-menu',
            /navbar-nav/,
            /navbar-default/,
            /carousel-inner/,
            /modal/,
            /fade/,
            /in/,
            /-effect/,
            /hashtags-item/,
            /jumbotron--404/
          ]
        },
        files: {
          'style.css': ['index.html']
        }
      }
    },
    watch: {
      options: {
        atBegin: true,
        interrupt: false,
        livereload: true,
        spawn: false
      },
      html: {
        files: ['dev/**/*.html'],
        tasks: ['htmlmin:dev']
      },
      sass: {
        files: ['dev/**/*.scss'],
        tasks: ['sass', 'postcss:dev']
      }
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['htmlmin:dist', 'sass:dist', 'uncss:dist', 'postcss:dist']);
  grunt.registerTask('full', ['imagemin', 'default']);
  grunt.registerTask('sync', ['browserSync', 'watch']);
};
