/*
 * Make It Retina v1.2 - jQuery Plugin
 * http://www.steviostudio.it
 *
 * Cristoforo Stevio Cervino
 * Date: 15/02/2013
 */
(function($) {
	$.makeItRetina = function(options) {
		var config = {
			'retinaSuffix':'_2x',
			'checkExistenceBefore':true,
			'checkifAlreadyRetina':true,
			'retinaBackgrounds':true,
			'retinaBackgroundsTags':'*',
			'tryEavenforExternalImages':false,
			'consoleDebugMessages':false,
			'makeItRetinaAnyway':false
		};
		if(options)$.extend(config,options);
		var retina = window.devicePixelRatio > 1 ? true : false;
		$.fn.changeWithRetina = function(url,type){
			switch(type){
				case 'img':
					this.attr('src',url);
				break;
				case 'background':
					this.css({
						'background-image':'url('+url+')'
					});
				break;
			}
		}
		$.fn.addSuffix = function(config,type){
			var $this = this;
			switch(type){
				case 'img':
					var url = $this.attr('src');
				break;
				case 'background':
					var url = $this.css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '');
				break;
			}
			var initialurl = url;
			var urlArray = url.split(".");
			if(config.checkifAlreadyRetina){
				if((urlArray[urlArray.length-2].indexOf(config.retinaSuffix)) >= 0){
					if(config.consoleDebugMessages){
						console.log('The image '+url+' is already retina;');
					}
				}
			}
			urlArray[urlArray.length-1] = (config.retinaSuffix)+'.'+urlArray[urlArray.length-1];
			urlArray[urlArray.length-2] = urlArray[urlArray.length-2]+urlArray[urlArray.length-1];
			urlArray.splice(urlArray.length-1);
			url = urlArray.join('.');
			if(config.checkExistenceBefore){
				$.ajax({
					url:url,
					type:'HEAD',
					async:true,
					error:function(){
						if(config.consoleDebugMessages){
							console.log('Can not load the image: '+initialurl+'.');
						}
					},
					success: function(){
						$this.changeWithRetina(url,type);
					}
				});
			}else{
				$this.changeWithRetina(url,type);
			}
		};
		$.fn.makeMeRetina = function(config,type) {
			switch(type){
				case 'img':
					var url = this.attr('src');
				break;
				case 'background':
					var url = this.css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '');
				break;
			}
			if((url.indexOf("http")) >= 0){
				if((url.indexOf(location.origin)) >= 0){
					this.addSuffix(config,type);
				}else{
					if(config.tryEavenforExternalImages){
						this.addSuffix(config,type);
					}
				}
			}else{
				this.addSuffix(config,type);
			}
		};
		if(retina || (config.makeItRetinaAnyway == true)){
			$(window).load(function() {
				$('img').each(function(index){
					if($(this).is(':visible')){
						$(this).css({
							width:$(this).width(),
							height:$(this).height()
						});
					}else{
						$img2 = $(this).clone();
						$img2.hide().appendTo('body');
						$(this).css({
							width:$img2.width(),
							height:$img2.height()
						});
						$img2.remove();
					}
					var est = $(this).attr('src');
					if (est != 'none'){
						$(this).makeMeRetina(config,'img');
					}
				});
				if(config.retinaBackgrounds){
					$(config.retinaBackgroundsTags).each(function(i, e) {
						var url = $(e).css('background-image');
						var bgsize = $(e).css('background-size');
						if (url != 'none' && ((url.indexOf("http"))>0)){
							if(bgsize == 'auto'){
								var url = $(e).css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '');
								var bgImg = $('<img />');
								bgImg.hide().attr('src',url);
								$('body').append(bgImg);
								bgImg.bind('load',function(){
									$(this).attr('data-height',$(this).height());
									$(this).attr('data-width',$(this).width());
									var dimensioni = $(this).attr('data-width')+'px '+$(this).attr('data-height')+'px';
									$(e).css({
										'background-size':dimensioni
									}).makeMeRetina(config,'background');
									$(this).remove();
								});
							}else{
								$(e).makeMeRetina(config,'background');
							}
						}
					});
				}
			});
		}
	}
})(jQuery);