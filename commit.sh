#!/bin/bash
message="Auto commit $(date)"
git checkout -- .
cd ..
git add . --all
git commit -m "$message"
git push origin master
cd formann-loyt
